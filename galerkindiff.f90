! ============================================================================
! Name        : 2dgalerkindiff.f90
! Author      : Babatunde Oni
! Version     :
! Copyright   :
! Description : Test vdiff on 2D problem
!               u=((k*pi)/gama))*cos(gama*t)*sin(k*pi*x)*cos(j*pi*z)
!               u_t = -p_x = (k*pi)*cos(gama*t)*sin(k*pi*x)*cos(j*pi*z)
!               w=((j*pi)/gama))*sin(gama*t)*cos(k*pi*x)*sin(j*pi*z)
!               w_t = -p_z = (k*pi)*cos(gama*t)*cos(k*pi*x)*sin(j*pi*z)
!               gama = pi*sqrt(k**2 + j**2)
!
! ============================================================================

program galerkindiff2d

use vdiff

implicit none

double precision, dimension(:,:), allocatable :: Mbx
double precision, dimension(:,:), allocatable :: Dbx
double precision, dimension(:,:), allocatable :: Mbz
double precision, dimension(:,:), allocatable :: Dbz
double precision, dimension(:,:), allocatable :: u
double precision, dimension(:,:), allocatable :: w
double precision, dimension(:,:), allocatable :: ut
double precision, dimension(:,:), allocatable :: us
double precision, dimension(:,:), allocatable :: wt
double precision, dimension(:,:), allocatable :: pt
double precision, dimension(:,:), allocatable :: ps
double precision, dimension(:,:), allocatable :: ws

double precision, dimension(:), allocatable :: x
double precision, dimension(:), allocatable :: z
double precision :: tottx
double precision :: tottz
double precision :: cflx
double precision :: cflz
double precision :: size
double precision :: sizew
double precision :: hx
double precision :: hz
double precision :: oldhx
double precision :: oldhz
double precision :: dt
double precision :: rate
double precision :: eror
double precision :: erorw
double precision :: erorold
double precision :: eroroldw
double precision :: kphi
double precision :: jphi
double precision :: gama
double precision :: uloc
double precision :: wloc
double precision :: t
!
integer :: nx
integer :: nz
integer :: mz
integer :: mx
integer :: px
integer :: pz
integer :: it
integer :: itz
integer :: inx
integer :: inz
integer :: nxsteps
integer :: nzsteps
integer :: j
integer :: qt
integer :: ngx
integer :: ngz
integer :: k

!
kphi =20.5d0*ACOS(-1.d0)
jphi =20.5d0*ACOS(-1.d0)
gama = sqrt(kphi**2 + jphi**2)
tottx=100.d0

!
do px =2,2
    print '(a,i4)', "degree=",2*px+1
    qt=4*px+3
!
    do ngx=0,0
    ngz=ngx
!
    print '(a,i4)', "nghost=2",ngx
!
    do inx=1,15

        select case (px)

        case (1)
            nx=20*(inx+3)
            nz=20*(inz+3)
        case (2)
            nx=20*(inx+2)
            nz=20*(inz+2)
        end select
!
      nxsteps=150*nx
     
!
      mx=nx+2*ngx+1
      mz=nz+2*ngz+1
!
      allocate(Mbx(2*px+2,mx),Dbx(4*px+3,mx),Mbz(2*pz+2,mz),Dbz(4*pz+3,mz))
      allocate(x(mx),u(mx,mz),w(mx,mz),z(mz),ut(mx,mz),us(mx,mz),pt(mx,mz),ps(mx,mz),ws(mx,mz),wt(mx,mz))
      
!
      hx=1.d0/DBLE(nx)
      dt=tottx/DBLE(nxsteps)
      cflx=dt/hx
!
      hz=1.d0/DBLE(nz)
      cflz=dt/hz
      
!! change dtx to just dt
!!!!!!!!!!!!!!!!!!!!!!!!U SOLVE !!!!!!!!!!!!!!!
     do k =1,mz
      do j=1,mx
        x(j)=DBLE(j-ngx-1)*hx
        z(k)=DBLE(k-ngz-1)*hz
        call exsol(uloc,x(j),z(k),0.d0,0)
        u(j,:)=uloc
      end do
       u(:,k)=uloc
     end do
    
    do j=1,mx
     do k =1,mz
        x(j)=DBLE(j-ngx-1)*hx
        z(k)=DBLE(k-ngz-1)*hz
        call dblusol(wloc,z(k),x(j),0.d0,0)
        w(k,:)=wloc
      end do
       w(:,j)=wloc
     end do
      call setup1(Mbx,Dbx,px,nx,mx,ngx)
      call setup1(Mbz,Dbz,pz,nz,mz,ngz)
     
      do it=1,1
        call taylorstep
       end do

!
      
      eror= 0.d0
      size = 0.d0
      t = tottx
      eror= 0.d0
      erorw=0.d0
      sizew = 0.d0
      
!
   do k=ngz+1,nz+ngz+1
     do j=ngx+1,nx+ngx+1
        call exsol(uloc,x(j),z(k),0.d0,0)
        eror = eror+hx*(u(j,k)-uloc**2)
        size = size+hx*(uloc**2)
     end do
   end do
 !!!!!!!!!!!!!! w !!!!!!!!!!!!!!!!!!!!!!!  
    do j=ngx+1,nx+ngx+1
     do k=ngz+1,nz+ngz+1
        call dblusol(wloc,z(k),x(j),0.d0,0)
        erorw = erorw+hz*(w(j,k)-wloc**2)
        sizew = sizew+hz*(wloc**2)
     end do
   end do
!!!!!!!!! u !!!!!!!!!!!!!!!!!!!!!!!!   
     eror = sqrt(eror/size)
     if (inx ==1) then
        print '(a, f6.2, a, e9.3)',"t=",t," relerr of u=",eror
     else
        rate=log(erorold/eror)/log(oldhx/hx)
        print '(a, f6.2, a, e9.3, a, f5.2)',"t=",t," relerr of u=",eror," rate=",rate
     end if
     erorold = eror
     oldhx=hx
!!!!!!!!!!! w !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      erorw = sqrt(erorw/sizew)
     if (inz ==1) then
        print '(a, f6.2, a, e9.3)',"t=",t," relerr of w=",erorw
     else
        rate=log(eroroldw/erorw)/log(oldhz/hz)
        print '(a, f6.2, a, e9.3, a, f5.2)',"t=",t," relerr of u=",eror," rate=",rate
     end if
     eroroldw = erorw
     oldhz=hz
     deallocate (Mbx,Dbx,Mbz,Dbz)
     deallocate(x,u,w,z,ut,us,pt,ps,ws,wt)
     
  end do
 end do
end do




!
contains
!only one taylor step change to just taylor step
  subroutine taylorstep 
!
  integer :: jt
  double precision :: dfx,dfz
!
  ut=u
  wt=w
  dfx=cflx
  dfz=cflz
  do jt=1,qt
    call uwp
    u=u+dfx*ut
    dfx=dfx*cflx/DBLE(jt+1)
!
    w=w+dfz*wt
    dfz=dfz*cflz/DBLE(jt+1)
  end do
!
  end subroutine taylorstep

 subroutine uwp
!
  integer :: kx,jx,kkx,kdx,info,kz,kkz,jz,kdz
 
  us=0.d0
  ps=0.d0
  kdx=2*px+2

do kz = 1,mz
  do kx=1,mx
     do jx=1,4*px+3
!qx()=u(:,kz)
      kkx=kx+jx-kdx
	    if ((kkx >0).and.(kkx <= mx)) then
           us(kkx,kz)=us(kkx,kz) - Dbx(jx,kx)*ut(kx,kz)
           ps(kkx,kz)=ps(kkx,kz) - Dbx(jx,kx)*ut(kx,kz) 
        end if
     end do
   end do
  us(ngx+1,kz)=us(ngx+1,kz)-u(ngx+1,kz)  ! x flux

  
 end do
  call DPBTRS('U',mz,2*px+1,1,Mbx,2*px+2,us,mz,info)
  if (info /= 0) then
    print *,'Trouble from DPBTRS in dudt: info=',info
    stop
  end if
  pt = -us
  ut = -ps
 
  ws=0.d0
  ps=0.d0
  kdz=2*pz+2

do kx = 1,mx
  do kz=1,mz
     do jz=1,4*pz+3
!qx()=u(:,kz)
      kkz=kz+jz-kdz
	    if ((kkz >0).and.(kkz <= mz)) then
           ws(kx,kkz)=ws(kx,kkz) - Dbz(jz,kz)*wt(kx,kz)
           ps(kkz,kx)=ps(kkz,kx) - Dbz(jz,kz)*wt(kx,kz)
        end if
     end do
   end do
  ws(ngz+1,kx)=ws(ngz+1,kx)-w(ngz+1,kx)
  
 end do
  call DPBTRS('U',mx,2*pz+1,1,Mbz,2*pz+2,ws,mx,info)
  if (info /= 0) then
    print *,'Trouble from DPBTRS in dudt: info=',info
    stop
  end if
  pt = -ws
  wt = -ps
 

end subroutine uwp


 subroutine exsol(uloc,x,z,t,id)
!
  integer, intent(IN) :: id
  double precision, intent(OUT) :: uloc
  double precision, intent(IN) :: x,z,t
!
  if(id==0) then
    uloc=(((kphi)/gama))*cos(gama*t)*sin(kphi*x)*cos(jphi*z)
  else
    print *,"Illegal value for id in exsol"
    stop
  end if

!
 end subroutine exsol
 
 subroutine dblusol(wloc,x,z,t,id)
!
  integer, intent(IN) :: id
  double precision, intent(OUT) :: wloc
  double precision, intent(IN) :: x,z,t
!
  if(id==0) then
    wloc=(((kphi)/gama))*cos(gama*t)*sin(kphi*x)*cos(jphi*z)
  else
    print *,"Illegal value for id in exsol"
    stop
  end if

!
 end subroutine dblusol
 

end program galerkindiff2d
