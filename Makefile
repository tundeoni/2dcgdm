# ============================================================================
# Name        : Makefile
# Author      : Babatunde Oni
# Version     :
# Copyright   : Your copyright notice
# Description : Makefile for Hello World in Fortran
# ============================================================================
SRC90    = SpecFuns.f90 vdiff1d.f90 galerkindiff.f90  

   
OBJ90    = SpecFuns.o vdiff1d.o galerkindiff.o

OBJ77    = 
   
FFLAGS=-O4 

FC=gfortran 

LALIBS= -llapack -lblas

test1d.x:	$(OBJ90) $(OBJ77)
	$(FC) -o test2d.x $(OBJ90) $(OBJ77) $(LALIBS)  

$(OBJ90):	$(SRC90)
	$(FC) $(FFLAGS) -c $*.f90 -o -g -Wall $@

$(OBJ77):	$(SRC77)
	$(FC) $(FFLAGS) -c $*.f -o $@


