MODULE vdiff

! Module for variational difference methods

USE SpecFuns

IMPLICIT NONE

CONTAINS

SUBROUTINE vdiff1(Mx,Dx,p,jf,frac)
!
! Compute the extended mass and derivative matrices for variational
! difference methods with local degree-2p+1 polynomials - note
! that here we take h=1
!
! If jf=0 integrate over the whole cell (p,p+1)
! If jf=-1 integrate from (p+1-frac,p+1)
! If jf=1 integrate from (p,p+frac)
!
! Mx,Dx (j,k) = integral of product of Lagrange functions and derivatives
! associated with nodes j and k. Nodes p,p+1 are the endpoints of the interval.
!
  INTEGER, INTENT(IN) :: p,jf
  DOUBLE PRECISION, INTENT(IN) :: frac
  DOUBLE PRECISION, DIMENSION(0:2*p+1,0:2*p+1), INTENT(OUT) :: Mx,Dx
  DOUBLE PRECISION, DIMENSION(0:2*p+1) :: w,x ! Gauss weights and nodes
  DOUBLE PRECISION, DIMENSION(0:2*p+1) :: phi,om,dphi
  DOUBLE PRECISION :: xj,xk
  INTEGER :: j,k,r
!
  Mx=0.d0
  Dx=0.d0
  CALL GaussQCofs(x,w,2*p+1)

  SELECT CASE (jf)

  CASE (0)
!
  DO j=0,2*p+1
    x(j)=(x(j)+1.d0)/2.d0
    w(j)=w(j)/2.d0
  END DO
!
  CASE (-1)
!
  DO j=0,2*p+1
    x(j)=1.d0+frac*(x(j)-1.d0)/2.d0
    w(j)=frac*w(j)/2.d0
  END DO
!
  CASE (1)
!
  DO j=0,2*p+1
    x(j)=frac*(x(j)+1.d0)/2.d0
    w(j)=frac*w(j)/2.d0
  END DO
!
  END SELECT
!
  phi=1.d0
  dphi=0.d0
  DO k=0,2*p+1
    DO j=0,2*p+1
      xj=x(k)-DBLE(j-p)
      phi(k)=phi(k)*xj
      dphi(k)=dphi(k)+1.d0/xj
    END DO
    dphi(k)=dphi(k)*phi(k)
  END DO
!
  om=1.d0
  DO j=0,2*p+1
    DO k=0,2*p+1
      IF (k /= j) THEN
        om(j)=om(j)*DBLE(j-k)
      END IF
    END DO
  END DO
!
  DO j=0,2*p+1
    DO k=0,2*p+1
      DO r=0,2*p+1
        xj=x(r)-DBLE(j-p)
        xk=x(r)-DBLE(k-p)
        Mx(j,k)=Mx(j,k)+w(r)*phi(r)*phi(r)/(om(j)*xj*om(k)*xk)
        Dx(j,k)=Dx(j,k)+w(r)*phi(r)*(dphi(r)*xk-phi(r))/  &
                (om(j)*xj*om(k)*xk*xk)
      END DO
    END DO
  END DO
!
END SUBROUTINE vdiff1

SUBROUTINE setup1(Mb,Db,p,n,m,nghost)
!
! Setup the banded mass and derivative matrices - then factor
! the mass matrix
!
! Here n=number of cells - m=#Dofs=n+2*nghost+1
!
! nghost=# of ghost basis elements on each side
! remaining elements are obtained by extrapolation
!
  INTEGER, INTENT(IN) :: p,n,m,nghost
  DOUBLE PRECISION, DIMENSION(2*p+2,m), INTENT(OUT) :: Mb
  DOUBLE PRECISION, DIMENSION(4*p+3,m), INTENT(OUT) :: Db
!
  INTEGER :: j,k,info,jt,kt,jx,kx,jj,kk,kd,kex
  DOUBLE PRECISION, DIMENSION(0:2*p+1,0:2*p+1) :: Dx,Mx
  DOUBLE PRECISION, DIMENSION(p,2*p+2) :: Xmat
  DOUBLE PRECISION, DIMENSION(2*p+2,n+2*p+1) :: Mb0
  DOUBLE PRECISION, DIMENSION(4*p+3,n+2*p+1) :: Db0
!
  IF (n < 2*p+2) THEN
    PRINT *,' n > 2p+1 required'
    STOP
  END IF
  IF (m /= (n+2*nghost+1)) THEN
    PRINT *,'m=n+2*nghost+1 required'
    STOP
  END IF
  IF (nghost > p) THEN
    PRINT *,'nghost <= p required '
    STOP
  END IF
  kd=2*p+1
!
  Mb0=0.d0
  Db0=0.d0
  Mb=0.d0
  Db=0.d0
  IF (nghost < p) THEN
    CALL extrap(Xmat,p)
  END IF
!
  CALL vdiff1(Mx,Dx,p,0,1.d0)
!
   DO jx=1,n  ! Loop over cells and build the matrices
              ! Note that the left node is at (jx-(p+1))h
              ! Index 1 -> node -ph
    DO j=0,2*p+1
      jt=jx+j
      DO k=0,2*p+1
        kt=jx+k
        Db0(kd+jt-kt+1,kt)=Db0(kd+jt-kt+1,kt)+Dx(j,k)
        IF (k >= j) THEN
          Mb0(kd+jt-kt+1,kt)=Mb0(kd+jt-kt+1,kt)+Mx(j,k)
        END IF
      END DO
    END DO
  END DO
!
  IF (nghost==p) THEN
    Mb=Mb0
    Db=Db0
  ELSE
    kex=p-nghost
    DO k=1,m
      jj=MAX(1,k-(2*p+1))
      kk=MIN(m,k+2*p+1)
      DO j=jj,kk
        Db(kd+j-k+1,k)=Db0(kd+j-k+1,k+kex)
!
        IF ((k < (2*p+3)).AND.(j < (2*p+3))) THEN
          jx=MAX(1,j+kex-2*p-1)
          kx=MAX(1,k+kex-2*p-1)
          DO kt=jx,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Db0(kd+j+kex-kt+1,kt)
          END DO
          DO jt=kx,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kex+1-jt,2*p+3-j)*Db0(kd+jt-kex-k+1,k+kex)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Xmat(kex+1-jt,2*p+3-j)*Db0(kd+jt-kt+1,kt)
          END DO
          END DO
        END IF
!
        IF ((k > (m-2*p-2)).AND.(j > (m-2*p-2))) THEN
          jx=MIN(kex,j+2*p+1-m)
          kx=MIN(kex,k+2*p+1-m)
          DO kt=1,jx
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Db0(kd+j-m-kt+1,m+kex+kt)
          END DO
          DO jt=1,kx
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(jt,j-m+2*p+2)*Db0(kd+jt+m-k+1,k+kex)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Xmat(jt,j-m+2*p+2)*Db0(kd+jt-kt+1,m+kex+kt)
          END DO
          END DO
        END IF
!
        IF (k >= j) THEN
!
        Mb(kd+j-k+1,k)=Mb0(kd+j-k+1,k+kex)
!
        IF ((k < (2*p+3)).AND.(j < (2*p+3))) THEN
          jx=MAX(1,j+kex-2*p-1)
          kx=MAX(1,k+kex-2*p-1)
          DO kt=jx,kex
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Mb0(kd-j-kex+kt+1,j+kex)
          END DO
          DO jt=kx,kex
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-jt,2*p+3-j)*Mb0(kd+jt-kex-k+1,k+kex)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            IF (kt >= jt) THEN
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Xmat(kex+1-jt,2*p+3-j)*Mb0(kd+jt-kt+1,kt)
            ELSE
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Xmat(kex+1-jt,2*p+3-j)*Mb0(kd+kt-jt+1,jt)
            END IF
          END DO
          END DO
        END IF
!
        IF ((k > (m-2*p-2)).AND.(j > (m-2*p-2))) THEN
          jx=MIN(kex,j+2*p+1-m)
          kx=MIN(kex,k+2*p+1-m)
          DO kt=1,jx
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Mb0(kd+j-m-kt+1,m+kex+kt)
          END DO
          DO jt=1,kx
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(jt,j-m+2*p+2)*Mb0(kd-jt-m+k+1,m+kex+jt)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            IF (kt >= jt) THEN
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Xmat(jt,j-m+2*p+2)*Mb0(kd+jt-kt+1,m+kex+kt)
            ELSE
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Xmat(jt,j-m+2*p+2)*Mb0(kd+kt-jt+1,m+kex+jt)
            END IF
          END DO
          END DO
        END IF
!
        END IF
      END DO
    END DO
!
  END IF
!
  CALL DPBTRF('U',m,kd,Mb,kd+1,info)
  IF (info /= 0) THEN
    PRINT *,'Trouble from dpbtrf in setup1: info=',info
    STOP
  END IF
!
END SUBROUTINE setup1

SUBROUTINE setup1_maxord(Mb,Db,p,n,m,nghost)
!
! Setup the banded mass and derivative matrices - then factor
! the mass matrix
!
! Here n=number of cells - m=#Dofs=n+2*nghost+1
!
! nghost=# of ghost basis elements on each side
! remaining elements are obtained by extrapolation
!
  INTEGER, INTENT(IN) :: p,n,m,nghost
  DOUBLE PRECISION, DIMENSION(2*p+2,m), INTENT(OUT) :: Mb
  DOUBLE PRECISION, DIMENSION(4*p+3,m), INTENT(OUT) :: Db
!
  INTEGER :: j,k,info,jt,kt,jx,kx,jj,kk,kd,kex
  DOUBLE PRECISION, DIMENSION(0:2*p+1,0:2*p+1) :: Dx,Mx
  DOUBLE PRECISION, DIMENSION(p,2*p+2) :: Xmat
  DOUBLE PRECISION, DIMENSION(2*p+2,n+2*p+1) :: Mb0
  DOUBLE PRECISION, DIMENSION(4*p+3,n+2*p+1) :: Db0
!
  IF (n < 2*p+2) THEN
    PRINT *,' n > 2p+1 required'
    STOP
  END IF
  IF (m /= (n+2*nghost+1)) THEN
    PRINT *,'m=n+2*nghost+1 required'
    STOP
  END IF
  IF (nghost > p) THEN
    PRINT *,'nghost <= p required '
    STOP
  END IF
  kd=2*p+1
!
  Mb0=0.d0
  Db0=0.d0
  Mb=0.d0
  Db=0.d0
  IF (nghost < p) THEN
    CALL extrap(Xmat,p)
  END IF
!
  CALL vdiff1(Mx,Dx,p,0,1.d0)
!
   DO jx=1,n   ! Loop over cells and build the matrices
              ! Note that the left node is at (jx-(p+1))h
              ! Index 1 -> node -ph
    DO j=0,2*p+1
      jt=jx+j
      DO k=0,2*p+1
        kt=jx+k
        Db0(kd+jt-kt+1,kt)=Db0(kd+jt-kt+1,kt)+Dx(j,k)
        IF (k >= j) THEN
          Mb0(kd+jt-kt+1,kt)=Mb0(kd+jt-kt+1,kt)+Mx(j,k)
        END IF
      END DO
    END DO
  END DO
!
  IF (nghost==p) THEN
    Mb=Mb0
    Db=Db0
  ELSE
    kex=p-nghost
    DO k=1,m
      jj=MAX(1,k-(2*p+1))
      kk=MIN(m,k+2*p+1)
      DO j=jj,kk
        Db(kd+j-k+1,k)=Db0(kd+j-k+1,k+kex)
!
        IF ((k < (2*p+3)).AND.(j < (2*p+3))) THEN
          jx=MAX(1,j+kex-2*p-1)
          kx=MAX(1,k+kex-2*p-1)
          DO kt=jx,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Db0(kd+j+kex-kt+1,kt)
          END DO
          DO jt=kx,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kex+1-jt,2*p+3-j)*Db0(kd+jt-kex-k+1,k+kex)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Xmat(kex+1-jt,2*p+3-j)*Db0(kd+jt-kt+1,kt)
          END DO
          END DO
        END IF
!
        IF ((k > (m-2*p-2)).AND.(j > (m-2*p-2))) THEN
          jx=MIN(kex,j+2*p+1-m)
          kx=MIN(kex,k+2*p+1-m)
          DO kt=1,jx
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Db0(kd+j-m-kt+1,m+kex+kt)
          END DO
          DO jt=1,kx
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(jt,j-m+2*p+2)*Db0(kd+jt+m-k+1,k+kex)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Xmat(jt,j-m+2*p+2)*Db0(kd+jt-kt+1,m+kex+kt)
          END DO
          END DO
        END IF
!
        IF (k >= j) THEN
!
        Mb(kd+j-k+1,k)=Mb0(kd+j-k+1,k+kex)
!
        IF ((k < (2*p+3)).AND.(j < (2*p+3))) THEN
          jx=MAX(1,j+kex-2*p-1)
          kx=MAX(1,k+kex-2*p-1)
          DO kt=jx,kex
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Mb0(kd-j-kex+kt+1,j+kex)
          END DO
          DO jt=kx,kex
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-jt,2*p+3-j)*Mb0(kd+jt-kex-k+1,k+kex)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            IF (kt >= jt) THEN
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Xmat(kex+1-jt,2*p+3-j)*Mb0(kd+jt-kt+1,kt)
            ELSE
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Xmat(kex+1-jt,2*p+3-j)*Mb0(kd+kt-jt+1,jt)
            END IF
          END DO
          END DO
        END IF
!
        IF ((k > (m-2*p-2)).AND.(j > (m-2*p-2))) THEN
          jx=MIN(kex,j+2*p+1-m)
          kx=MIN(kex,k+2*p+1-m)
          DO kt=1,jx
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Mb0(kd+j-m-kt+1,m+kex+kt)
          END DO
          DO jt=1,kx
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(jt,j-m+2*p+2)*Mb0(kd-jt-m+k+1,m+kex+jt)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            IF (kt >= jt) THEN
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Xmat(jt,j-m+2*p+2)*Mb0(kd+jt-kt+1,m+kex+kt)
            ELSE
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Xmat(jt,j-m+2*p+2)*Mb0(kd+kt-jt+1,m+kex+jt)
            END IF
          END DO
          END DO
        END IF
!
        END IF
      END DO
    END DO
!
  END IF
!
  CALL DPBTRF('U',m,kd,Mb,kd+1,info)
  IF (info /= 0) THEN
    PRINT *,'Trouble from dpbtrf in setup1: info=',info
    STOP
  END IF
!
END SUBROUTINE setup1_maxord

SUBROUTINE setup1_embed(Mb,Db,Vleft,Vright,p,n,m,nghost,fleft,fright)
!
! Setup the banded mass and derivative matrices - then factor
! the mass matrix - here we assume the first and last cells
! contain boundary points in their interior and that the numbers
! fleft, fright measure the fraction of the cell size which is
! active
!
! Here if h is the cell size in the interior then the size of
! the first and last cells are fleft*h and fright*h. This
! implies that the physical boundary locations are
!
! x(0)+(1-fleft)*h   x(n)+(fright-1)*h
!
! Here n=number of cells - m=#Dofs=n+2*nghost+1
!
! nghost=# of ghost basis elements on each side
! remaining elements are obtained by extrapolation
!
  INTEGER, INTENT(IN) :: p,n,m,nghost
  DOUBLE PRECISION, INTENT(IN) :: fleft,fright
  DOUBLE PRECISION, DIMENSION(2*p+2,m), INTENT(OUT) :: Mb
  DOUBLE PRECISION, DIMENSION(4*p+3,m), INTENT(OUT) :: Db
  DOUBLE PRECISION, DIMENSION(2*p+2), INTENT(OUT) :: Vleft,Vright
!
  INTEGER :: j,k,info,jt,kt,jx,kx,jj,kk,kd,kex
  DOUBLE PRECISION, DIMENSION(0:2*p+1,0:2*p+1) :: Dx,Mx,Dxl,Mxl,Dxr,Mxr
  DOUBLE PRECISION, DIMENSION(p,2*p+2) :: Xmat
  DOUBLE PRECISION, DIMENSION(2*p+2,n+2*p+1) :: Mb0
  DOUBLE PRECISION, DIMENSION(4*p+3,n+2*p+1) :: Db0
!
  IF (n < 2*p+2) THEN
    PRINT *,' n > 2p+1 required'
    STOP
  END IF
  IF (m /= (n+2*nghost+1)) THEN
    PRINT *,'m=n+2*nghost+1 required'
    STOP
  END IF
  IF (nghost > p) THEN
    PRINT *,'nghost <= p required '
    STOP
  END IF
  IF ((fleft >= 1.D0).OR.(fleft <= 0.d0)) THEN
    PRINT *,' 0 < fleft < 1 required '
    STOP
  END IF
  IF ((fright >= 1.D0).OR.(fright <= 0.d0)) THEN
    PRINT *,' 0 < fright < 1 required '
    STOP
  END IF
  kd=2*p+1
!
  Mb0=0.d0
  Db0=0.d0
  Mb=0.d0
  Db=0.d0
!
  IF (nghost < p) THEN
    CALL extrap(Xmat,p)
  END IF
!
  CALL vdiff1(Mx,Dx,p,0,1.d0)
  CALL vdiff1(Mxl,Dxl,p,-1,fleft)
  CALL vdiff1(Mxr,Dxr,p,1,fright)
!
   DO jx=1,n   ! Loop over cells and build the matrices
              ! Note that the left node is at (jx-(p+1))h
              ! Index 1 -> node -ph
    DO j=0,2*p+1
      jt=jx+j
      DO k=0,2*p+1
        kt=jx+k
!
        IF (jx==1) THEN
!
        Db0(kd+jt-kt+1,kt)=Db0(kd+jt-kt+1,kt)+Dxl(j,k)
        IF (k >= j) THEN
          Mb0(kd+jt-kt+1,kt)=Mb0(kd+jt-kt+1,kt)+Mxl(j,k)
        END IF
!
        ELSE IF (jx==n) THEN
!
        Db0(kd+jt-kt+1,kt)=Db0(kd+jt-kt+1,kt)+Dxr(j,k)
        IF (k >= j) THEN
          Mb0(kd+jt-kt+1,kt)=Mb0(kd+jt-kt+1,kt)+Mxr(j,k)
        END IF
!
        ELSE
!
        Db0(kd+jt-kt+1,kt)=Db0(kd+jt-kt+1,kt)+Dx(j,k)
        IF (k >= j) THEN
          Mb0(kd+jt-kt+1,kt)=Mb0(kd+jt-kt+1,kt)+Mx(j,k)
        END IF
!
        END IF
!
      END DO
    END DO
  END DO
!
  IF (nghost==p) THEN
    Mb=Mb0
    Db=Db0
  ELSE
    kex=p-nghost
    DO k=1,m
      jj=MAX(1,k-(2*p+1))
      kk=MIN(m,k+2*p+1)
      DO j=jj,kk
        Db(kd+j-k+1,k)=Db0(kd+j-k+1,k+kex)
!
        IF ((k < (2*p+3)).AND.(j < (2*p+3))) THEN
          jx=MAX(1,j+kex-2*p-1)
          kx=MAX(1,k+kex-2*p-1)
          DO kt=jx,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Db0(kd+j+kex-kt+1,kt)
          END DO
          DO jt=kx,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kex+1-jt,2*p+3-j)*Db0(kd+jt-kex-k+1,k+kex)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Xmat(kex+1-jt,2*p+3-j)*Db0(kd+jt-kt+1,kt)
          END DO
          END DO
        END IF
!
        IF ((k > (m-2*p-2)).AND.(j > (m-2*p-2))) THEN
          jx=MIN(kex,j+2*p+1-m)
          kx=MIN(kex,k+2*p+1-m)
          DO kt=1,jx
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Db0(kd+j-m-kt+1,m+kex+kt)
          END DO
          DO jt=1,kx
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(jt,j-m+2*p+2)*Db0(kd+jt+m-k+1,k+kex)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            Db(kd+j-k+1,k)=Db(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Xmat(jt,j-m+2*p+2)*Db0(kd+jt-kt+1,m+kex+kt)
          END DO
          END DO
        END IF
!
        IF (k >= j) THEN
!
        Mb(kd+j-k+1,k)=Mb0(kd+j-k+1,k+kex)
!
        IF ((k < (2*p+3)).AND.(j < (2*p+3))) THEN
          jx=MAX(1,j+kex-2*p-1)
          kx=MAX(1,k+kex-2*p-1)
          DO kt=jx,kex
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Mb0(kd-j-kex+kt+1,j+kex)
          END DO
          DO jt=kx,kex
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-jt,2*p+3-j)*Mb0(kd+jt-kex-k+1,k+kex)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            IF (kt >= jt) THEN
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Xmat(kex+1-jt,2*p+3-j)*Mb0(kd+jt-kt+1,kt)
            ELSE
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kex+1-kt,2*p+3-k)*Xmat(kex+1-jt,2*p+3-j)*Mb0(kd+kt-jt+1,jt)
            END IF
          END DO
          END DO
        END IF
!
        IF ((k > (m-2*p-2)).AND.(j > (m-2*p-2))) THEN
          jx=MIN(kex,j+2*p+1-m)
          kx=MIN(kex,k+2*p+1-m)
          DO kt=1,jx
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Mb0(kd+j-m-kt+1,m+kex+kt)
          END DO
          DO jt=1,kx
            Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(jt,j-m+2*p+2)*Mb0(kd-jt-m+k+1,m+kex+jt)
          END DO
          DO jt=1,kex
          DO kt=1,kex
            IF (kt >= jt) THEN
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Xmat(jt,j-m+2*p+2)*Mb0(kd+jt-kt+1,m+kex+kt)
            ELSE
              Mb(kd+j-k+1,k)=Mb(kd+j-k+1,k)+Xmat(kt,k-m+2*p+2)*Xmat(jt,j-m+2*p+2)*Mb0(kd+kt-jt+1,m+kex+jt)
            END IF
          END DO
          END DO
        END IF
!
        END IF
      END DO
    END DO
!
  END IF
!
  CALL DPBTRF('U',m,kd,Mb,kd+1,info)
  IF (info /= 0) THEN
    PRINT *,'Trouble from dpbtrf in setup1_embed: info=',info
    STOP
  END IF
!
  CALL interp(Vleft,p,nghost,fleft,-1)
  CALL interp(Vright,p,nghost,fright,1)
!
END SUBROUTINE setup1_embed

SUBROUTINE extrap(Xmat,p)
!
! Order 2p+2 extrapolation to p ghost points
!
INTEGER, INTENT(IN) :: p
DOUBLE PRECISION, DIMENSION(p,2*p+2), INTENT(OUT) :: Xmat
!
INTEGER :: k,r,s
DOUBLE PRECISION :: xs,xr,xk
!
Xmat=1.d0

DO s=1,2*p+2
  xs=DBLE(s-2*p-2)
  DO r=1,p
   xr=DBLE(r)
   DO k=1,2*p+2
     IF (k /= s) THEN
       xk=DBLE(k-2*p-2)
       Xmat(r,s)=Xmat(r,s)*(xr-xk)/(xs-xk)
     END IF
   END DO
  END DO
END DO

END SUBROUTINE extrap

SUBROUTINE interp(Vmat,p,nghost,f,jf)
!
! Order 2p+2 interpolation
!
! If jf=-1 the point is located at ng+2-f
!
! If jf=1 the point is located at 2p+1+f-nghost
!
INTEGER, INTENT(IN) :: p,nghost,jf
DOUBLE PRECISION, INTENT(IN) :: f
DOUBLE PRECISION, DIMENSION(2*p+2), INTENT(OUT) :: Vmat
!
INTEGER :: k,r,s
DOUBLE PRECISION :: xs,xr,xk
!
Vmat=1.d0

IF (jf == -1) THEN
  xr=DBLE(nghost+2)-f
ELSEIF (jf==1) THEN
  xr=DBLE(2*p+1-nghost)+f
ELSE
  PRINT *,' must have jf=1 or -1 in interp'
  STOP
END IF

DO s=1,2*p+2
  xs=DBLE(s)
  DO k=1,2*p+2
    IF (k /= s) THEN
      xk=DBLE(k)
      Vmat(s)=Vmat(s)*(xr-xk)/(xs-xk)
    END IF
  END DO
END DO

END SUBROUTINE interp


END MODULE vdiff




